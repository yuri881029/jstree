/**
 * Created by yuri on 29/07/16.
 */
var a = [
    { id: 1, parentId: null, name: 'root' },
    { id: 2, parentId: 1, name: 'ch1' },
    { id: 3, parentId: 2, name: 'ch2' },
    { id: 4, parentId: 2, name: 'ch2' },
    { id: 5, parentId: 2, name: 'ch2' },
    { id: 6, parentId: 1, name: 'ch1' },
    { id: 7, parentId: 6, name: 'ch2' },
    { id: 8, parentId: 6, name: 'ch2' },
    { id: 9, parentId: 6, name: 'ch2' },
    { id: 10, parentId: 9, name: 'ch3' }
];
function Queue() {
    this._oldestIndex = 1;
    this._newestIndex = 1;
    this._storage = {};
}

Queue.prototype.size = function() {
    return this._newestIndex - this._oldestIndex;
};

Queue.prototype.enqueue = function(data) {
    this._storage[this._newestIndex] = data;
    this._newestIndex++;
};

Queue.prototype.dequeue = function() {
    var oldestIndex = this._oldestIndex,
        newestIndex = this._newestIndex,
        deletedData;

    if (oldestIndex !== newestIndex) {
        deletedData = this._storage[oldestIndex];
        delete this._storage[oldestIndex];
        this._oldestIndex++;

        return deletedData;
    }
};
//Node class
function Node(data) {
    this.data = data;
    this.parent = null;
    this.children = [];
}
//Tree class
function Tree(data) {
    var node = new Node(data);
    this._root = node;
}
//Tree methods
Tree.prototype.add = function(data, toData, traversal) {
    var child = new Node(data),
        parent = null,
        callback = function(node) {
            if (node.data.id === toData) {
                parent = node;
            }
        };
    this.contains(callback, traversal);
    if (parent) {
        parent.children.push(child);
        child.parent = parent.data.id;
    } else if(child.parent!==null) {
       return false;//throw new Error('Cannot add node to a non-existent parent.');
    }
};
function findRoot(node){
    //find the property parentId setted as null
  return node.parentId===null;
}

Tree.prototype.contains = function(callback, traversal) {
    traversal.call(this, callback);
};

Tree.prototype.traverseDF = function(callback) {
    // this is a recurse and immediately-invoking function
    (function recurse(currentNode) {
        // step 2
        for (var i = 0, length = currentNode.children.length; i < length; i++) {
            // step 3
            recurse(currentNode.children[i]);
        }
        // step 4
        callback(currentNode);
        // step 1
    })(this._root);

};
Tree.prototype.traverseBF = function(callback) {
        var queue = new Queue();

        queue.enqueue(this._root);

        currentTree = queue.dequeue();

        while(currentTree){
            for (var i = 0, length = currentTree.children.length; i < length; i++) {
                queue.enqueue(currentTree.children[i]);
            }

            callback(currentTree);
            currentTree = queue.dequeue();
        }


};

function createTree(data){
    var root=data.find(findRoot), map=[];
    if (root){
        var MyTree = new Tree(root);
        map.push(root.id);
        for (var i = 0; i < data.length; i += 1)
        {
            var item=data[i];
            if(MyTree.add(item,item.parentId,MyTree.traverseDF)!==false)
            {
               map.push(item.id);
            }
        }
        return MyTree;
    }else{
        throw new Error("Can't create a tree with out root")
    }
};
function list(node) {
    var newLi= document.createElement('li');
    var parent=node.parent;
    var name=document.createElement("input");
    name.setAttribute('type','text');
    name.setAttribute('value',node.data.name);
    newLi.setAttribute('id',node.data.id);
    newLi.appendChild(name);

    if(node.children.length>0)
    {
        var newUl= document.createElement('ul');
        newUl.setAttribute('id',"ul"+node.data.id);
        newLi.appendChild(newUl);
    }

    if(parent!==null){

        document.getElementById("ul"+parent).appendChild(newLi);

    }else{
        var setRoot= document.createElement('ul');
        setRoot.setAttribute('id',"root"+node.data.id);
        setRoot.appendChild(newLi);
        document.body.appendChild(setRoot);
    }

};
function printTree()
{
    var tree=createTree(a);
    console.log(tree.traverseBF(list));
}


